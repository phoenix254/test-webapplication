<?php

class PropertyController
{

    public function __construct()
    {
    }

    public function getMostSearched()
    {
        try {
            header('Content-type: application/json');

            $simulatedDataSource = $this->getDataSource();$simulatedDataSource = file_get_contents('public/assets/most_searched.json');
            $properties = json_decode($simulatedDataSource, true);

            //TODO perform an manipulation of the data from here

            echo $simulatedDataSource;
            return $simulatedDataSource;
        }catch (Exception $exception){
            echo json_encode([
                "error" => $exception->getMessage()
            ]);
        }
    }

    public function getLastestProperties(){
        try {
            header('Content-type: application/json');


            $simulatedDataSource = $this->getDataSource();
            $properties = json_decode($simulatedDataSource, true);

            //TODO perform an manipulation of the data from here

            echo $simulatedDataSource;
            return $simulatedDataSource;
        }catch (Exception $exception){
            echo json_encode([
                "error" => $exception->getMessage()
            ]);
        }
    }

    public function getDataSource(){
        return $simulatedDataSource = file_get_contents('public/assets/most_searched.json');
    }

}