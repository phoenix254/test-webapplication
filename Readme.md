##SPA Rent Demo
__Rent application__ currently has code that simulates a real application
the endpoints being used to simulate the REST Api are dummy ones.

If need be to complete the application the will provide a starting point for 
the Api.

The Android application also makes calls to the Api endpoints and gets the same feeds
The code on the main page is meant to act as a template for the 
actual application to be built

The code also has __//TODO's__ that are mean to show points in the code
that one can use to make the application complete

![screenshot](public/images/screen_2.png)

The application also comes short in the following:  
    * The application is not fully responsive and this can be done with time  
    * Currently the implementation is using a dummy data source
    
For a full application this can be modified to use a database with ease

The site can be reached here [__Rent application link__](https://rent.jamesgathu.com/)

__NB: The site is currently only optimized for ipad, ipad pro and medium sized computers__

Thank you.