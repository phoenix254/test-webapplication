<?php

require "vendor/autoload.php";
require "Controllers/PropertyController.php";

$router = new \Bramus\Router\Router();


$router->get('/', function () {
    $source = new PropertyController();

    $data = $source->getDataSource();
    require __DIR__."/public/home.php";
});


$router->mount('/api', function () use ($router) {

    $router->get('/', function () {
        require __DIR__."/public/api_index.php";
    });

    $router->get('/properties/searched', 'PropertyController@getMostSearched');

    $router->get('/properties/latest', 'PropertyController@getLastestProperties');

});


$router->set404(function () {
    header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    require __DIR__."/public/miss.php";
});


$router->run();