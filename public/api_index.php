<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Rent</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous"/>
    <link href="https://fonts.googleapis.com/css?family=Lato|Open+Sans" rel="stylesheet">
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <link rel="stylesheet" href="<?php parse_url("plugins/main.css") ?>"/>
</head>
<body>
<div class="container">
    <div class="row justify-content-md-center" style="margin-top: 50px">
        <div class="col-md-8 ">
            <h2>SPA Test Demo [Api]</h2>

            <p>
                This is meant to be a dummy api for the demo, it works for both the <strong>Android application</strong> and<br>
                    the <strong>Web application</strong> at <a href="http://rent.jswiftdev.com">rent.jswiftdev.com</a> <br>
                <strong>Try the following end points:</strong>
            </p>

            <ul class="list-group">
                <li class="list-group-item"><a href="http://rent.jswiftdev.com/api/properties/latest">http://rent.jswiftdev.com/api/properties/latest</a>
                </li>
                <li class="list-group-item"><a href="http://rent.jswiftdev.com/api/properties/searched">http://rent.jswiftdev.com/api/properties/searched</a>
                </li>
            </ul>
        </div>


    </div>
</div>

<!--scripts-->
<script src="plugins/jquery/jquery.js"></script>
<script src="https://use.fontawesome.com/8afa749b0d.js"></script>
<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
<script src="plugins/bootstrap/js/bootstrap.js"></script>
</body>
</html>