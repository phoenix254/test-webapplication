<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Rent</title>

    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css"/>
    <link href="https://fonts.googleapis.com/css?family=Lato|Open+Sans" rel="stylesheet">
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <link rel="stylesheet" href="plugins/main.css"/>
</head>
<body>


<!--nav bar-->
<nav style="margin-top: 20px">
    <div class="container">
        <div class="row">
            <p style="color: #f5b63d; font-size: 1.75em; font-weight:600">Rent</p>
            <div class="col-md-8 push-sm-0 push-md-4" style="margin-top: 0.6em">
                <ul class="footer-menu" id="nav-menu">
                    <li><strong class="align-middle">How it Works</strong></li>
                    <li><i class="fa fa-user-circle fa-2x align-middle"></i></li>
                    <li>
                        <button class="btn header-button align-middle">List your property
                        </button>
                    </li>
                </ul>
            </div>

        </div>
    </div>
</nav>


<!--header with box-->
<div class="container-fluid" id="header">
    <!--title-->
    <div class="row justify-content-md-center">
        <div class="col-md-4">
            <p id="header-title">Rent a House or Office Space</p>
        </div>
    </div>

    <!--box-->
    <div class="row justify-content-md-center">
        <div class="col-md-8 col-sm-8" id="top-box">

            <div class="row justify-content-md-center">
                <div class="col-md-9 col-sm-9">
                    <div class="row">
                        <ul class="horizontal-style">
                            <li class="menus-border"><a href="#">Location</a></li>
                            <li class="menus-border"><a href="#">Estate</a></li>
                            <li><a href="#">Max-Price</a></li>
                        </ul>
                    </div>


                    <div class="row">
                        <div class="col-md-4 list-checkboxes">
                            <input type="checkbox" name="apartment" id="apartment">
                            <label for="apartment">Apartment</label>

                        </div>

                        <div class="col-md-4">
                            <input type="checkbox" name="room" id="room">
                            <label for="room">Room</label>
                        </div>

                        <div class="col-md-4">
                            <button class="btn" id="search-button">
                                <i class="fa fa-search" aria-hidden="true" style="margin-right: 10px"></i>
                                Search
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="container" style="margin-top: 30px">
    <!--Our latest properties-->
    <div class="content-wrapper">
        <strong style="font-size: 1.3em">Our Latest Properties</strong>
        <hr class="underline">

        <div class="row">
            <!--single card-->
            <!--implementation with a backend will require a loop here-->
            <?php foreach (json_decode($data, true) as $property) { ?> <!--TODO would be @foreach in laravel-->
                <div class="col-md-4 property-card">
                    <img class="card-image" src="<?php echo $property["image_url"] ?>">
                    <p><?php echo $property["title"] ?>
                        <br>
                        <span>
                            <i class="fa fa-map-marker" style="color: #f5b63d; margin-right: 5px"></i><small><?php echo $property["place"] ?></small>
                        </span>
                        <br>
                        <strong style="color: #ccc51c">KES. <?php echo $property["cost"] ?> pm</strong>
                    </p>
                </div>
            <?php } ?> <!--TODO would be @endforeach-->
        </div>

        <!--From most searched areas-->
        <div class="content-wrapper">
            <strong style="font-size: 1.3em">From Most Searched Areas</strong>
            <hr class="underline">
            <div class="row">
                <?php foreach (json_decode($data, true) as $property) { ?> <!--TODO would be @foreach in laravel-->
                    <div class="col-md-4 property-card">
                        <img class="card-image" src="<?php echo $property["image_url"] ?>">
                        <p><?php echo $property["title"] ?>
                            <br>
                            <span>
                            <i class="fa fa-map-marker" style="color: #f5b63d; margin-right: 5px"></i><small><?php echo $property["place"] ?></small>
                        </span>
                            <br>
                            <strong style="color: #ccc51c">KES. <?php echo $property["cost"] ?> pm</strong>
                        </p>
                    </div>
                <?php } ?> <!--TODO would be @endforeach-->
            </div>
        </div>
    </div>
</div>

<footer style="background-color: #584848;">
    <div class="row justify-content-md-center">
        <div class="col-md-8" style="text-align: center; color: white; padding-top: 70px">
            <p>Telephone : (+254) 070 7686364 | E-mail : support@rent.com<br>
                9am - 5pm Monday - Friday</p>
            <br>

            <ul class="footer-menu">
                <li>Help |</li>
                <li>How it Works |</li>
                <li>Contact Us |</li>
                <li>Q&A</li>
            </ul>

            <br>
            <ul class="footer-menu" id="social">
                <li><i class="fa fa-facebook-square fa-2x"></i></li>
                <li><i class="fa fa-twitter fa-2x"></i></li>
                <li><i class="fa fa-instagram fa-2x"></i></li>
            </ul>

            <br>

            <p>Copyright Rent 2017 All Right Reserved</p>

            <br>
            <br>
        </div>
    </div>
</footer>


<!--page footer-->


<!--scripts-->
<script src="plugins/jquery/jquery.js"></script>
<script src="https://use.fontawesome.com/8afa749b0d.js"></script>
<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
<script src="plugins/bootstrap/js/bootstrap.js"></script>
</body>
</html>



